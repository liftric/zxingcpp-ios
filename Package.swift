// swift-tools-version:5.4
import PackageDescription

let package = Package(
    name: "ZXingCpp",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "ZXingCpp",
            targets: ["ZXingCpp"]
        )
    ],
    targets: [
        .binaryTarget(
            name: "ZXingCpp",
            url: "https://gitlab.com/liftric/zxingcpp-ios/-/raw/main/releases/xcframework/zxingcpp-ios-v2.0.0.zip",
            checksum: "06d9b09878c33077b431d2f75aabd2306b3c55a85c815053279f7c5f8a885269"
        )
    ]
)
